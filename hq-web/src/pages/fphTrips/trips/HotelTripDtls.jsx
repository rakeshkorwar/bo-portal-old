import React, { Component } from 'react'


class HotelTripDtls extends Component {

    constructor(props) {
        super(props)
        this.state = {
            content: ''

        }

    }
    render() {

        return (

            this.props.htl.map(
                ele => {

                    return (
                        ele.type === "hotel_booking"
                            ?
                            <React.Fragment>
                                <div className="destination">Hotel in {ele.city_name} - {ele.hotel_name}</div>
                                <table className="dataTable5">
                                    <thead>
                                        <th width="25%">Check in</th>
                                        <th width="25%">Check out</th>
                                        <th width="25%">Duration</th>
                                        <th width="25%">Rooms</th>

                                    </thead>
                                    <tbody>
                                        {
                                            <tr key={ele.id}>
                                                <td>{ele.check_in_date}</td>
                                                <td>{ele.check_out_date}</td>
                                                <td></td>
                                                <td>{ele.room_count} {ele.room_type}</td>
                                            </tr>
                                        }
                                    </tbody>
                                </table> </React.Fragment> :
                            <React.Fragment>
                                <h4 className="intabTTl-sub">Hotel booking details : {ele.no_of_adult} Adults</h4>
                                <table className="dataTable5">
                                    <thead>
                                    <th width="30%">Room Type</th>
                                    <th width="15%">Supplier</th>
                                    <th width="20%">Voucher</th>
                                    <th width="20%">Number of Travellers</th>
                                    <th width="15%">Status</th>

                                    </thead>
                                    <tbody>
                                        {ele.room_info.map(info => <tr key={info.id}>
                                            <td>{info.room_name} </td>
                                            <td>{info.supplier_name}</td>
                                            <td>{info.voucher_number}</td>
                                            <td></td>
                                            <td>{info.status}</td>

                                        </tr>
                                        )}

                                    </tbody>

                                </table></React.Fragment>)

                }
            )

        )
    }


}

export default HotelTripDtls;