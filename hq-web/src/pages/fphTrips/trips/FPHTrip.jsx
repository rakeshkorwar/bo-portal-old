import React, { Component } from 'react'
import FlightTripDtls from './FlightTripDtls'
import HotelTripDtls from './HotelTripDtls'

class FPHTrip extends Component {

    constructor(props) {
        super(props)
        this.state = {
            content:""
        }
        
    }

    
    
    render() {
        
        return (
           
            <>
                <h4 className="intabTTl">Flight Itinerary</h4>
                <FlightTripDtls flt={this.props.fltDtl}/>
                <h4 className="intabTTl">Hotel Itinerary</h4>
                <HotelTripDtls htl={this.props.htlDtl}/>
                <div>
                <h4 className="intabTTl">Inclusion And Extra</h4>
                   <span>{this.props.htlDtl[1].inclusion}</span>
                </div>
           
            </>

        )
    }


}

export default FPHTrip;