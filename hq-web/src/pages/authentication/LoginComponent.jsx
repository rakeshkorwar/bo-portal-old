import React, { Component } from 'react'
import AuthenticationService from '../../services/authentication/AuthenticationService';
import { Link } from 'react-router-dom'
import logo from '../../assets/images/logo.gif';
export const URL_COMPANY = 'company'

export const USER_AUTH_DATA = 'userAuthData'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
export const USER_LOGIN_URL = 'userLoginUrl'

class LoginComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]
                    : event.target.value
            }
        )
    }
    addDocCookie(response){
        document.cookie = 'ct-auth' + " = "+response.data['ct-auth']
        document.cookie = 'Apache' + " = "+response.data['Apache']
        document.cookie = 'userid' + "=" + response.data['userid']
        document.cookie = 'ct-auth-preferences' + "=" + response.data['ct-auth-preferences']
        document.cookie = 'currency-pref' + "=" + response.data['currency-pref']
        document.cookie = '_session_id' + "=" + response.data['_session_id']
        document.cookie = 'ct-auth' + "=" + response.data['ct-auth']
        document.cookie = 'usermisc' + "=" + response.data['usermisc']
    }

    loginClicked() {
        //in28minutes,dummy
         if(this.state.username!=='' && this.state.password!==''){
             AuthenticationService.registerSuccessfulLogin(this.state.username,this.state.password)
            .then(response => {
                var loginResp=response.data;
                console.log('Login Status..........'+loginResp.status)
                if(loginResp.status ==='Success'){
                    localStorage.setItem(USER_AUTH_DATA,loginResp);
                    localStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME,this.state.username);
                    this.addDocCookie(response);
                    this.props.history.push(`/hq/company`) 
                    this.setState({showSuccessMessage:true})
                    this.setState({hasLoginFailed:false})
                }else{
                    console.log('Login Failed..........'+loginResp.status)
                    this.setState({showSuccessMessage:false})
                    this.setState({hasLoginFailed:true})
                }
            })
            .catch(function (error) {
                if (error.response) {
               console.log(error.message);  
                } 
                });
         }
         else {
            console.log('notlogin')
             this.setState({showSuccessMessage:false})
             this.setState({hasLoginFailed:true})
         }

       

    }

    render() {
        return (
            
               
                <div className="container">             
                <div className="loginForm form-group">
                <Link to="/"><img src={logo} className="App-logo" alt="logo" /></Link>
                <h4>User Login</h4> 
                    {/*<ShowInvalidCredentials hasLoginFailed={this.state.hasLoginFailed}/>*/}
                    {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>}
                    {this.state.showSuccessMessage && <div>Login Sucessful</div>}
                    <div className="loginInput">
                    {/*<ShowLoginSuccessMessage showSuccessMessage={this.state.showSuccessMessage}/>*/}
                   <label>User Name </label> <input type="text" placeholder="Enter User Name" name="username" value={this.state.username} onChange={this.handleChange} />
                   </div>
                   <div className="loginInput">
                   <label>Password </label> <input type="password" placeholder="Enter Password" name="password" value={this.state.password} onChange={this.handleChange} />
                    </div>
                    <button className="btn btn-primary btn-xs" onClick={this.loginClicked}>Login</button>
            </div>
            </div>
        )
    }
}

export default LoginComponent