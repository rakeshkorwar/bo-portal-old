import { Route, Redirect } from 'react-router-dom'
import React, { Component } from 'react'
import Icon from '../../component/Icon';
import Header from '../../pages/common/Header'
import Footer from '../../pages/common/Footer'

class AccessDenied extends Component{
    render() {
        return (
            <>
            <Header />
            <div className="MainContainer cont-Box text-center denied pt-50">
            <Icon color="#cda11e" size={40} icon="warning" />
                <h3 class="secondary-cwr">Access Denied..</h3>
                <div className="container">
                    Please contact with Administrator....
                </div>
                </div>
                <Footer />
            </>
        )
    }
}

export default AccessDenied

