import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import AuthenticationService from '../../services/authentication/AuthenticationService';

class AuthenticatedRoute extends Component {
    render() {
        console.log('isUserLoggedIn')
        if (AuthenticationService.isUserLoggedIn()) {
            console.log('LoggedIn')
            return <Route {...this.props} />
        } else {
            console.log('not LoggedIn')
            return <Redirect to="/hq/login" />
        }

    }
}

export default AuthenticatedRoute