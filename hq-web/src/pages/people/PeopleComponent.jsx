import React, { Component } from 'react'
//import PeopleDataService from '../../services/people/PeopleDataService.js';
import { Link } from 'react-router-dom';
import Header from '../common/Header'
import Footer from '../common/Footer'
class PeopleComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fName: '',
            lName: '',
            usrName: ''
        }

    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleSubmit(event) {
        alert('An essay was submitted: ' + this.state.value);
        event.preventDefault();
    }



    render() {
        console.log("Cookies.get('name')")

        return (
            <>
                <Header />
                <div className="MainContainer">

                    <div className="container">
                        <h3>People</h3>
                        <div className="container">
                            <label>
                                First Name:
            <input type="text" value={this.state.fName} onChange={this.handleChange} />
                            </label>
                            <label>
                                Last Name:
            <input type="text" value={this.state.lName} onChange={this.handleChange} />
                            </label>
                            <label>
                                UserName:
            <input type="text" value={this.state.usrName} onChange={this.handleChange} />
                            </label>
                            <button type="button" onClick={this.handleSubmit} className="btn btn-primary"><b>Search</b></button>
                        </div>

                        <br />
                        <table>
                        <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Mobile Number</th>
                                <th>DOB</th>
                            </tr>
                            {

                                <tr>
                                    <td><Link to={'/component/ShowPeopleComponent/'}>Lakshmi narayana </Link> </td>
                                    <td>lakshmi.narayana@cleartrip.com</td>
                                    <td>9014488878</td>
                                    <td>10/10/2019</td>
                                </tr>
                            }
                        </tbody>
                        </table>
                    </div>
                </div>
                <Footer />
            </>
        )
    }
}

export default PeopleComponent
