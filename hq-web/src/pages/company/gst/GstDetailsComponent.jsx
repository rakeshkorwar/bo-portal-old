import React, { Component } from "react";
import GstService from "../../../services/gst/GstService.js";
import Icon from '../../../component/Icon';
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer'
import loader from '../../../assets/images/loader.gif';

class GstDetailsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gstHolderName: "",
      gstHolderaddress: "",
      stateCode: "",
      stateName: "",
      gstinList: "false",
      selGSTNo: "",
      gstinfields: "false",
      filtyeredGstData: "",
      responseData: "",
      message: "",
      errorMsg:'',
      gstId: "",
      loading:'',
      delEve:'',
      editEve:'',
      permissions: [1234, 5678],
      addGST:false,
      gstResultData:this.props.gstData,
      addgstSuccessMsg:this.props.addgstmsg
    };
    this.handleChange = this.handleChange.bind(this);
    this.addRecordEvent = this.addRecordEvent.bind(this);
    this.deletEvent = this.deletEvent.bind(this);
    this.editEvent = this.editEvent.bind(this);
    this.backBtnEvent = this.backBtnEvent.bind(this);
    this.gstinListener = this.gstinListener.bind(this);
    //alert('gstData '+JSON.stringify(this.props.gstData));
    //alert('gstData '+JSON.stringify(this.props.gstCompanyName));
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  addRecordEvent(e) {
    const addGstpage = "ADD PAGE";

    this.props.gstDetailsHandler(addGstpage,this.state.gstResultData);
  }

  deletEvent(e) {
    this.state.loading='true';
    this.state.delEve='true';
    this.state.editEve='false';
    this.forceUpdate();
    const gstdelDetails = [
      {
        gst_number: this.state.selGSTNo
      }
    ];

    this.delGstReq = {
      domain_name: this.props.gstCompanyName,
      gst_details: gstdelDetails
    };
    this.deleteCompanyGSTReq(this.delGstReq);
  }

  deleteCompanyGSTReq(delGstReq) {
    this.state.message = "";
    GstService.deleteGstCompanyDetails(delGstReq).then(response => {
      const delGstResponse = JSON.parse(response.data);
      this.setState({ responseData: delGstResponse });

      console.log(
        "deleteGstCompanyDetails Res " + JSON.stringify(this.state.responseData)
      );

      if (
        this.state.responseData !== undefined &&
        this.state.responseData !== "" &&
        this.state.responseData.status !== undefined &&
        this.state.responseData.status !== "" &&
        this.state.responseData.status === 200
      ) {
        const msg='GST number ('+this.state.selGSTNo+ ') deleted successfully'
        this.setState({ message: msg });
        this.state.loading='false';
        this.state.gstinfields='false';
       let gstupdatedData = this.state.gstResultData.gst_details.filter
      (item => item.gst_number !== this.state.selGSTNo)
      this.state.gstResultData.gst_details=gstupdatedData;
        this.forceUpdate();
        //this.props.gstDetailsMsg(this.state.message);
      }else{
        this.state.loading='false';
        this.forceUpdate();
        this.setState({ errorMsg: "GST number not deleted  " });
      }
    });
  }

  editEvent(e) {
    this.state.loading='true';
    this.state.delEve='false';
    this.state.editEve='true';
    const gsteditDetails = [
      {
        id: this.state.gstId,
        gst_holder_address: this.state.gstHolderaddress,
        gst_holder_name: this.state.gstHolderName,
        gst_holder_state_code: this.state.stateCode,
        gst_holder_state_name: this.state.stateName,
        gst_number: this.state.selGSTNo
      }
    ];

    this.editReq = {
      domain_name: this.props.gstCompanyName,
      gst_details: gsteditDetails
    };

    this.editCompanyGSTReq(this.editReq);
  }

  editCompanyGSTReq(editRequest) {
    this.state.message = "";
    this.state.errorMsg = "";    
    this.state.loading='true';
    this.forceUpdate();
    GstService.editGstCompanyDetails(editRequest).then(response => {
      this.state.responseData = JSON.parse(response.data);
      console.log(
        "editGstCompanyDetails Res " + JSON.stringify(this.state.responseData)
      );
      if (
        this.state.responseData !== undefined &&
        this.state.responseData !== "" &&
        this.state.responseData.status !== undefined &&
        this.state.responseData.status !== "" &&
        this.state.responseData.status === 200
      ) {
        
        
        const msg='GST ('+this.state.selGSTNo+ ') details updated successfully'
        this.setState({ message: msg });
        this.state.loading='false';
        let gstupdatedData = this.state.gstResultData.gst_details.map(
          (gstres, sidx) => {
    
            if (this.state.selGSTNo !== gstres.gst_number)
              return gstres;
            return {
              ...gstres,
              id: this.state.gstId,
              gst_holder_address: this.state.gstHolderaddress,
              gst_holder_name: this.state.gstHolderName,
              gst_holder_state_code: this.state.stateCode,
              gst_holder_state_name: this.state.stateName,
              gst_number: this.state.selGSTNo
            };
          });
         
        this.state.gstResultData.gst_details=gstupdatedData;   
        this.forceUpdate();        
        //this.props.gstDetailsMsg(this.state.message);
      } else {
        this.state.loading='false';
        this.forceUpdate();
        this.setState({ errorMsg: "GST details not updated " })
        
      }
    });
  }

  backBtnEvent(e) {
    const backbtn = "SEARCH PAGE";
    this.props.gstDetailsHandler(backbtn);
  }

  componentWillMount() {
    if (
      this.props.gstData != undefined &&
      this.props.gstData.responsecode != undefined &&
      this.props.gstData.responsecode === 200 &&
      (this.props.gstData.gst_details != undefined &&
        this.props.gstData.gst_details.length > 0)
    ) {
      this.state.gstinList = "true";
    } else {
      this.state.gstinList = "false";
    }
  }

  gstinListener = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.state.selGSTNo = e.target.value;
    this.state.errorMsg='';
    this.state.message='';
    this.state.addgstSuccessMsg='';

    if (this.state.selGSTNo !== "" && this.state.selGSTNo !== undefined) {
      this.state.gstinfields = "true";
    } else {
      this.state.gstinfields = "false";
    }

    this.state.filtyeredGstData = this.state.gstResultData.gst_details.map(
      (gstResult, index) => {
        if (this.state.selGSTNo === gstResult.gst_number) {
          this.state.gstHolderName = gstResult.gst_holder_name;
          this.state.gstHolderaddress = gstResult.gst_holder_address;
          this.state.stateCode = gstResult.gst_holder_state_code;
          this.state.stateName = gstResult.gst_holder_state_name;
          this.state.gstId = gstResult.id;
          return gstResult;
        } else {
          return "";
        }
      }
    );
  };

  render() {
    const gst = this.state.permissions.find(x => x === 1234);
    if (gst){
        this.state.addGST = true;
    }
    return (
      <>
        <Header />
        <div className="MainContainer pageTtl">
          <h3>GST Record</h3>
        </div>
        <div className="MainContainer cont-Box">
        {this.state.errorMsg !== "" && this.state.errorMsg !== undefined && (
            <div className="alert alert-danger">{this.state.errorMsg}</div>
          )}

          {this.state.message !== "" && this.state.message !== undefined && (
            <div className="alert notification-success">{this.state.message}</div>
          )}

          {this.state.addgstSuccessMsg !== "" && this.state.addgstSuccessMsg !== undefined && (
            <div className="alert notification-success">{this.state.addgstSuccessMsg}</div>
          )}


        <div className="selectedComp">
        <label className="SubTitle">Domain name : <strong>{this.props.gstCompanyName}</strong></label>
                </div>
          <div className="form-group">
   

            <div className="row">
            <div className="col-5">
            <div className="row">
            {this.state.gstinList == "true" ? (
              <>
              <div className="col-4">
                {" "}
                <label>Select your GST</label>
              </div>
              <div className="col-6">
                <div className="custom-select-v3">
                <Icon
                    className="selectme"
                    color="#CAD6E3"
                    size={20}
                    icon="down-arrow"
                  />
                 
                      <select
                        name="selGSTNo"
                        name="selGSTNo"
                        value={this.state.selGSTNo}
                        onChange={this.gstinListener}
                      >
                        <option value="">-Select-</option>
                        {this.state.gstResultData.gst_details.map(
                          (gstResult, index) => (
                            <option key={index} value={gstResult.gst_number}>
                              {gstResult.gst_number}
                            </option>
                          )
                        )}
                      </select>
                   
                  
                </div>
                </div>
               </>
                
                  ) : (
                      ""
                    )}
            {gst===1234 && (
             <div className="col-1">
             <button
                  id="addbtnId"
                  type="button"
                  onClick={this.addRecordEvent}
                  className="btn btn-xs btn-primary"
                >
                  Add GST
                </button>
              </div>
              )}
   </div>

              </div>
            </div>

            {this.state.gstinfields == "true" ? (
              <>
                <div className="row">
                  <div className="col-5">
                    <div className="row">
                      <div className="col-4">
                        <label>GST holder name </label>
                      </div>
                      <div className="col-6">
                        <input
                          type="text"
                          name="gstHolderName"
                          onChange={this.handleChange}
                          value={this.state.gstHolderName}
                        />

                      </div>
                    </div>
                    <div className="row">
                      <div className="col-4">
                        <label>GST holder address </label>
                      </div>
                      <div className="col-6">
                        <input
                          type="text"
                          name="gstHolderaddress"
                          onChange={this.handleChange}
                          value={this.state.gstHolderaddress}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-5">
                    <div className="row">
                      <div className="col-3">
                        <label>State code </label>
                      </div>

                      <div className="col-6">
                        <input
                          type="text"
                          name="stateCode"
                          onChange={this.handleChange}
                          value={this.state.stateCode}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-3">
                        <label>State name </label>
                      </div>
                      <div className="col-6">
                        <input
                          type="text"
                          name="stateName"
                          onChange={this.handleChange}
                          value={this.state.stateName}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="btnSec">
                  <button
                    id="backbtnId"
                    type="button"
                    onClick={this.backBtnEvent}
                    className="btn btn-xs btn-default"
                  >
                    Cancel
                  </button>
                  <button
                    id="delbtnId"
                    type="button"
                    onClick={this.deletEvent}
                    className="btn btn-xs btn-primary"
                  >
                    Delete GST
                 
                  </button>
                  <span className="ldrPart">{(this.state.loading==='true' && this.state.delEve==='true') && <img src={loader} className="App-logo" alt="loader" />}</span>
                  <button
                    id="editbtnId"
                    type="button"
                    onClick={this.editEvent}
                    className="btn btn-xs btn-primary"
                  >
                    Edit GST                   
                  </button>
                  <span className="ldrPart">{(this.state.loading==='true' &&  this.state.editEve==='true') && <img src={loader} className="App-logo" alt="loader" />}</span>
                 
                </div>
              </>
            ) : (
                ""
              )}
          </div>
        </div>
        <Footer />
      </>
    );
  }
}
export default GstDetailsComponent;
