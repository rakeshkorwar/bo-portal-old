import React, { Component } from 'react'
import Icon from '../../../component/Icon';
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer'
import CompanyService from '../../../services/company/CompanyService';
import loader from '../../../assets/images/loader.gif';

class EditCompanyComponent extends React.Component {
    constructor(props) {
        super(props)
        var companyNameVal = this.props.detailsPagedata.name
        var companyIdVal = this.props.detailsPagedata.companyId
        var domainNm = this.props.detailsPagedata.domain_name
        var cityVal = this.props.detailsPagedata.city
        var addressVal = this.props.detailsPagedata.address
        var addressTypeVal = this.props.detailsPagedata.addressType
        var contactVal = this.props.detailsPagedata.contactNum
        var contactTypeVal = this.props.detailsPagedata.contactType
        var emailVal = this.props.detailsPagedata.emailId
        var emailTypeVal = this.props.detailsPagedata.emailType
        var contact_data_val= this.props.detailsPagedata.contact_data
        /* var confDropDown = []
        {
            this.props.detailsPagedata.company_configs.length > 0 && this.props.detailsPagedata.company_configs.map((person, index) => (
                confDropDown.push({ 'config_name': person.config_name, 'config_value': person.config_value })
            ))
        } */
        /* var configIdAndLabelMap = []
        {
            this.props.detailsPagedata.company_configs.length > 0 && this.props.detailsPagedata.company_configs.map((person, index) => (
                configIdAndLabelMap.push({ 'id': person.id, 'config_name': person.config_name })
            ))
        } */

        this.state = {
            arrayData: this.props.detailsPagedata,
            companyId: companyIdVal,
            companyName: companyNameVal,
            domainName: domainNm,
            emailId: emailVal,
            emailTypes: emailTypeVal,
            contactNum: contactVal,
            contactTypes: contactTypeVal,
            address: addressVal,
            addressTypes: addressTypeVal,
            city: cityVal,
            selConfig: '',
            selConfigValue: '',
            flyerDtls: [{id:"", config_name: "", config_value: ""}],
            newconfDropdown: '',
            idAndNameMap:'',
            editCompConfdropDownId:'',
            contact_data_json:contact_data_val,
            errorMsg: '',
            errorMsgList:[],
            successMsgList:[],
            loading:'' 
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    newConfigDrop1 = idx => evt => {
        let newFlyerDtls = this.state.flyerDtls.map((flyerDtl, sidx) => {
            if (idx !== sidx) return flyerDtl;
            return { ...flyerDtl, config_name: evt.target.value };
        });
        this.setState({ flyerDtls: newFlyerDtls });
    }
    newConfigDrop2 = idx => evt => {
        let newFlyerDtls = this.state.flyerDtls.map((flyerDtl, sidx) => {
            if (idx !== sidx) return flyerDtl;
            return { ...flyerDtl, config_value: evt.target.value };
        });
        this.setState({ flyerDtls: newFlyerDtls });
    };
    handleAddFlyerDtl = () => {
        this.setState({
            flyerDtls: this.state.flyerDtls.concat([{id:"", config_name: "", config_value: "" }])
        });
    };
    handleRemoveFlyerDtl = idx => () => {
        this.setState({
            flyerDtls: this.state.flyerDtls.filter((s, sidx) => idx !== sidx)
        });
    };


    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, errorMsgList:[], successMsgList:[] });
    }

    handleChangeSelConf = (e) => {
        this.setState({ [e.target.name]: e.target.value });
        var confDropDown = []

        {
            this.state.arrayData.company_configs.length > 0 && this.props.detailsPagedata.company_configs.map((person, index) => (
                confDropDown.push({ 'config_name': person.config_name, 'config_value': person.config_value })
            ))
            this.state.newconfDropdown=confDropDown;
        }

        var configIdAndLabelMap = []
        {
            this.state.arrayData.company_configs.length > 0 && this.props.detailsPagedata.company_configs.map((person, index) => (
                configIdAndLabelMap.push({ 'id': person.id, 'config_name': person.config_name })
            ))

            this.state.idAndNameMap=configIdAndLabelMap;
        }

        if (e.target.value) {
            var keyMap = this.state.newconfDropdown.filter(function (itemData) {
                return itemData.config_name === e.target.value;
            });
            this.setState({ selConfigValue: keyMap[0].config_value });
            var keyMapData = this.state.idAndNameMap.filter(function (itemData) {
                return itemData.config_name === e.target.value;
            });
            this.setState({ editCompConfdropDownId: keyMapData[0].id });
        } else {
            this.setState({ selConfigValue: '' });
            this.setState({ editCompConfdropDownId: '' });
        }
    }

    handleSubmit(e) {
        this.state.loading='true';
        const { flyerDtls } = this.state;
      
        if(!this.checkValidations()){
          console.log("Invalid data");
          this.state.loading='false';
          this.forceUpdate();
        }else{
            if(this.state.contact_data_json !== undefined){
                // address data
                if(this.state.contact_data_json.addresses !== undefined && (this.state.address !== '' || this.state.addressTypes !== '')){
                    if(this.state.contact_data_json.addresses.length > 0){
                        this.state.contact_data_json.addresses[0].category=this.state.addressTypes
                        this.state.contact_data_json.addresses[0].street_address=this.state.address
                    }else{
                        this.state.contact_data_json.addresses=[{
                            category : this.state.addressTypes,
                            street_address:this.state.address
                        }]
                    }
                }
    
                // Email
                if(this.state.contact_data_json.emails !== undefined && (this.state.emailId !== '' || this.state.emailTypes !== '' )){
                    if(this.state.contact_data_json.emails.length >0){
                        this.state.contact_data_json.emails[0].category=this.state.emailTypes
                        this.state.contact_data_json.emails[0].email_id=this.state.emailId
                    }else{
                        this.state.contact_data_json.emails=[{
                            category : this.state.emailTypes,
                            email_id:this.state.emailId
                        }]
                    }
                }
    
                // Phone
                if(this.state.contact_data_json.phone_numbers !== undefined && (this.state.contactNum !== '' || this.state.contactTypes !== '')){
                    if(this.state.contact_data_json.phone_numbers.length >0){
                        this.state.contact_data_json.phone_numbers[0].category=this.state.contactTypes
                        this.state.contact_data_json.phone_numbers[0].phone_number_value=this.state.contactNum
                    }else{
                        this.state.contact_data_json.phone_numbers=[{
                            category : this.state.contactTypes,
                            phone_number_value:this.state.contactNum
                        }]
                    }
                }
            }

            var jsmTemp;
            var flyerData=[]
            for(jsmTemp of flyerDtls){
                if((jsmTemp.config_name !== '' && jsmTemp.config_name !== null) && (jsmTemp.config_value !== '' && jsmTemp.config_value !== null)){
                    flyerData=flyerData.concat([{id:"", config_name: jsmTemp.config_name, config_value: jsmTemp.config_value }]); 
                }
            }
            if(this.state.selConfig !== '' && this.state.selConfigValue !== ''){
                flyerData = flyerData.concat([{id:this.state.editCompConfdropDownId, config_name: this.state.selConfig, config_value: this.state.selConfigValue }])
            }
            
            this.companyEditInfo = {
                companyId: this.state.companyId,
                company: {
                    name: this.state.companyName.trim(),
                    city: this.state.city,
                    domain_name: this.state.domainName,
                    company_configs: flyerData,
                    
                    contact_data: {
                        emails: this.state.contact_data_json.emails,
                        phone_numbers: this.state.contact_data_json.phone_numbers,
                        addresses :this.state.contact_data_json.addresses
                        
                    }
                }
            }
            this.editCompanyDetailsReq(this.companyEditInfo);
        }
    }


    checkValidations(){
        var result=true;
        const { flyerDtls } = this.state;
        this.state.errorMsgList=[]
        this.state.successMsgList=[]
        // Company name
        if(this.state.companyName.trim() === '' || this.state.companyName.trim() === null){
            this.state.errorMsgList.push('Company name must be provided')
            result= false;
         }

         if(this.state.emailId !== '' && this.state.emailId !== null){
            let lastAtPos = this.state.emailId.lastIndexOf('@'); 
            let lastDotPos = this.state.emailId.lastIndexOf('.'); 
            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.emailId.indexOf('@@') == -1 && lastDotPos > 2 && (this.state.emailId.length - lastDotPos) > 2)){ 
                this.state.errorMsgList.push('Enter a valid email address')
                result= false;
            }
         }

         if(this.state.selConfig !== '' && this.state.selConfigValue === ''){
            this.state.errorMsgList.push('The Value of '+this.state.selConfig+' must be provided');
            result= false;
         }
         var jsm;
         for(jsm of flyerDtls){
             if((jsm.config_name === '' && jsm.config_value !== '') || (jsm.config_name !== '' && jsm.config_value === '')){
                this.state.errorMsgList.push("Combination of new config and value must be provided"); 
                result= false;
                break;
             }
         }
         return result;
    }

    editCompanyDetailsReq(companyEditInfo) {
        CompanyService.editCompanyDetails(companyEditInfo)
            .then(response => {
                const fetchCmpData = JSON.parse(response.data);
                this.setState({ fetchCmpData });

                if(fetchCmpData.status !== undefined && fetchCmpData.status === 200){

                    let updateData = this.state.arrayData.company_configs.map(
                        (comRes, sidx) => {
         
                          if (Number(this.state.editCompConfdropDownId) !== Number(comRes.id))
                            return comRes;
                          return {
                            ...comRes,
                            config_value: this.state.selConfigValue
                          };
                        });
                       
                      this.state.company_configs=updateData;             
                      this.state.arrayData.company_configs=this.state.company_configs;



                    this.state.successMsgList.push("Company details are updated successfully");
                    this.state.loading='false';
                    this.forceUpdate();
                }else{
                    this.state.errorMsgList.push("Company details not updated");
                    this.state.loading='false';
                    this.forceUpdate();
                }
            })
            .catch(this.setState({ fetchCmpData: '' }));
    }

    gotoSearchPage = () => {
        this.props.clickEditBackAddHandler('searchPage');
    }

    render() {
        const { channelName } = this.state
        return (
            <>
                <Header />
                <div className="MainContainer pageTtl">
                <h3>Edit Company Details</h3>
                </div>
                <div className="MainContainer cont-Box">
                {this.state.errorMsgList !== '' && this.state.errorMsgList.length > 0 && <div className="alert alert-danger">
                <Icon className="noticicon" color="#F4675F" size={16} icon="warning"/> {this.state.errorMsgList.map((item, key) => <span key={item}>{item}</span>)}
                </div>
                } 
                {this.state.successMsgList !== '' && this.state.successMsgList.length > 0 && <div className="alert alert-success">
                <Icon className="noticicon" color="#02AE79" size={16} icon="success"/> {this.state.successMsgList.map((item, key) => <span key={item}>{item}</span>)}
                </div>
                }    
                
                <div className="selectedComp">
                <label className="SubTitle">Domain name : <strong>{this.state.arrayData.domain_name}</strong></label>
                </div>                
                    
                    <div className="form-group">
                        <div className="row">
                            <div className="col-5">
                                <div className="row">
                                    <div className="col-3"><label>Company name </label><span className="required">*</span></div>
                                    <div className="col-5">
                                        <input type="text" name="companyName" value={this.state.companyName} placeholder="Enter company name" onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-3"><label>Company id </label></div>
                                    <div className="col-5">
                                        <input type="text" name="companyId" disabled={true} value={this.state.companyId} placeholder="Enter company id" onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-3"><label>Company city </label></div>
                                    <div className="col-5">
                                        <input type="text" name="city" value={this.state.city} onChange={this.handleChange} placeholder="Enter company city"/>
                                    </div>
                                </div>

                            </div>

                            <div className="col-5">
                                <div className="row">
                                    <div className="col-3"><label>Email </label></div>
                                    <div className="col-5">
                                        <input type="text" name="emailId" value={this.state.emailId} onChange={this.handleChange} placeholder="Enter email id"/>
                                    </div>
                                    <div className="col-4">
                                        <div className="custom-select-v3">
                                        <Icon className="selectme" color="#CAD6E3" size={20} icon="down-arrow" />
                                            <select name="emailTypes" name="emailTypes" value={this.state.emailTypes} onChange={this.handleChange} >
                                                <option value="">-Select-</option>
                                                <option value="personal">PERSONAL</option>
                                                <option value="other">OTHER</option>
                                                <option value="work">WORK</option>
                                                <option value="home">HOME</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-3"><label>Contact number</label></div>
                                    <div className="col-5">
                                        <input type="text" name="contactNum" value={this.state.contactNum} onChange={this.handleChange} placeholder="Enter contact number"/>
                                    </div>
                                    <div className="col-4">
                                        <div className="custom-select-v3">
                                        <Icon className="selectme" color="#CAD6E3" size={20} icon="down-arrow" />
                                            <select name="contactTypes" name="contactTypes" value={this.state.contactTypes} onChange={this.handleChange}>
                                                <option value="">-Select-</option>
                                                <option value="other">OTHER</option>
                                                <option value="landline">LANDLINE</option>
                                                <option value="property">PROPERTY</option>
                                                <option value="work">WORK</option>
                                                <option value="mobile">MOBILE</option>
                                            </select>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-3"><label>Address</label></div>
                                    <div className="col-5">
                                        <input type="text" name="address" value={this.state.address} onChange={this.handleChange} placeholder="Enter address"/>
                                    </div>
                                    <div className="col-4">
                                        <div className="custom-select-v3">
                                        <Icon className="selectme" color="#CAD6E3" size={20} icon="down-arrow" />
                                            <select name="addressTypes" name="addressTypes" value={this.state.addressTypes} onChange={this.handleChange}>
                                                <option value="">-Select-</option>
                                                <option value="other">OTHER</option>
                                                <option value="billing">BILLING</option>
                                                <option value="property">PROPERTY</option>
                                                <option value="work">WORK</option>
                                                <option value="home">HOME</option>
                                                <option value="shipping">SHIPPING</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12">
                                <label> Select the config</label>

                            </div>
                            <div className="col-3">
                                <div className="custom-select-v3">
                                <Icon className="selectme" color="#CAD6E3" size={20} icon="down-arrow" />
                                    <select name="selConfig" name="selConfig" value={this.state.selConfig} onChange={this.handleChangeSelConf}>
                                        <option value="">-Select-</option>
                                        {
                                            this.props.detailsPagedata.company_configs.length > 0 && this.props.detailsPagedata.company_configs.map((h, i) =>
                                                (<option key={i} value={h.config_name}>{h.config_name}</option>))
                                        }
                                    </select>
                                   
                                </div>
                            </div>
                            <div className="col-3">
                                <input type="text" name="selConfigValue" value={this.state.selConfigValue} onChange={this.handleChange} placeholder="Enter the config value"/>

                            </div>
                        </div>

                        <div className="addComp">
                            <div className="row">
                                <div className="col-6">


                                    <label>Add new configs:</label>
                                    {this.state.flyerDtls.map((flyerDtl, idx) => (

                                        <div className="flyerDtl" key={idx}>
                                            <div className="row">
                                                <div className="col-6">
                                                    <input
                                                        type="text"
                                                        placeholder={`Enter the new config name`}
                                                        value={flyerDtl.config_name}
                                                        onChange={this.newConfigDrop1(idx)}
                                                    />
                                                </div>
                                                <div className="col-6">
                                                    <input
                                                        type="text"
                                                        placeholder={`Enter the new config value`}
                                                        value={flyerDtl.config_value}
                                                        onChange={this.newConfigDrop2(idx)}
                                                    />
                                                </div>
                                                <div className="col-1 closeX">
                                                    <button
                                                        type="button"
                                                        onClick={this.handleRemoveFlyerDtl(idx)}
                                                        className="btnEvent closeme"
                                                    >
                                                        <Icon color="#4D6F93" size={20} icon="delete" />
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                                <div className="col-3 addmePlus">
                                    <button
                                        type="button"
                                        onClick={this.handleAddFlyerDtl}
                                        className="btnEvent addme"
                                    >
                                        <Icon color="#4D6F93" size={16} icon="add" />
                                    </button>
                                </div>
                            </div>
                        </div>


                        <div className="btnSec">
                            <button type="button" onClick={this.gotoSearchPage} className="btn btn-xs btn-default">Cancel</button>
                            <button type="button" onClick={this.handleSubmit} className="btn btn-xs btn-primary">Updates</button>
                            <span className="ldrPart">{this.state.loading==='true' && <img src={loader} className="App-logo" alt="loader" />}</span>
                        </div>
                    </div>
                </div>
                <Footer />
            </>
        )
    }
}
export default EditCompanyComponent