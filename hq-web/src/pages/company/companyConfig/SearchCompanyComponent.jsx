import React, { Component } from 'react'
import CompanyService from '../../../services/company/CompanyService.js';
import Icon from '../../../component/Icon';
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer';
import loader from '../../../assets/images/loader.gif';

class SearchCompanyComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            domainName: '',
            errorMsg: '',
            resData:{},
            loading:'' 
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, errorMsg :"" });
    }

    handleSubmit(e) {
        this.state.loading='true';
        this.setState({ errorMsg: '' })
        this.props.getDomainName(this.state.domainName.trim());
        if (this.state.domainName.trim() === '') {
            this.state.loading='false';
            this.forceUpdate();
            this.setState({ errorMsg: 'Domain name must be provided' })
        } else {
            this.searchRequest = {
                domain_name: this.state.domainName.trim(),
            }
            this.fetchCompanyDetails(this.searchRequest);
        }
    }

    fetchCompanyDetails(searchRequest) {
        CompanyService.fetchCompanyDetails(searchRequest)
            .then(response => {
                const fetchCmpData = JSON.parse(response.data);
                this.setState({ fetchCmpData });
                if(fetchCmpData !== undefined && fetchCmpData.status !== undefined){
                    if(fetchCmpData.status !== 200){
                        this.state.loading='false';
                        this.state.errorMsg = 'Searched Domain name does not exist';
                        this.forceUpdate();
                    }else{
                        this.props.clickSearchHandler(fetchCmpData);
                        this.state.loading='false';
                    }
                }else{
                    this.state.loading='false';
                    this.state.errorMsg = 'Searched Domain name does not exist';
                    this.forceUpdate();
                }
            })
            .catch(this.setState({ fetchCmpData: '' }));
    }

    render() {
        const { channelName } = this.state
        return (
            <>
                <Header />
                <div className="MainContainer pageTtl">
                <h3>Company Config Details</h3>
                </div>
                <div className="MainContainer cont-Box">
                    {this.state.errorMsg !== '' && <div className="alert alert-danger"><Icon className="noticicon" color="#F4675F" size={16} icon="warning"/> {this.state.errorMsg}</div>}                    
                    <div className="form-group">
                    <div className="row">
                            <div className="col-"><label>Domain name </label><span className="required">*</span></div>
                            <div className="col-2">
                                <input type="text" name="domainName" value={this.state.domainName} 
                                 onChange={this.handleChange} placeholder="Enter domain name"/>
                            </div>
                            <div className="col-2">
                                <button type="button" onClick={this.handleSubmit} className="btn btn-xs btn-primary"><b>Submit</b></button>
                                <span className="ldrPart">{this.state.loading==='true' && <img src={loader} className="App-logo" alt="loader" />}</span>
                            </div>
                            </div>
                    </div>

                </div>
                <Footer />
            </>
        )
    }
}
export default SearchCompanyComponent