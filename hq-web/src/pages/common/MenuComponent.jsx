import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '../../component/Icon';
import AuthenticationService from '../../services/authentication/AuthenticationService';
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

class MenuComponent extends Component {
    
    constructor(){
        super();
        this.redirect=this.redirect.bind(this);
        this.state = {
            displayMenu: false,
          };
     
       this.showDropdownMenu = this.showDropdownMenu.bind(this);
       this.hideDropdownMenu = this.hideDropdownMenu.bind(this);
     
     };
     showDropdownMenu(event) {
        event.preventDefault();
        this.setState({ displayMenu: true }, () => {
        document.addEventListener('click', this.hideDropdownMenu);
        });
      }
    
      hideDropdownMenu() {
        this.setState({ displayMenu: false }, () => {
          document.removeEventListener('click', this.hideDropdownMenu);
        });
    
      }
  
    redirect(event){
        event.preventDefault();
        if(event.target.id === 'myTrips'){
            window.location = "https://qa2.cleartrip.com/account/trips"
        }else if(event.target.id === 'account'){
            window.location = "https://qa2.cleartrip.com/account/profile?bcvr=true"
        }else if(event.target.id === 'tellUs'){
            window.location = "https://www.surveymonkey.com/r/L2Z9GZY?sm=YICZQSE2%2fOUdX9QuCvyfjDAEgqZg8YplUIK2YGoHrqI%3d"
        }else if(event.target.id === 'dashboard'){
            window.location = "https://qa2.cleartrip.com/hq"
        }else if(event.target.id === 'trips'){
            window.location = "https://qa2.cleartrip.com/hq/trips/"
        }else if(event.target.id === 'places'){
            window.location = "https://qa2.cleartrip.com/hq/places"
        }else if(event.target.id === 'gst'){
            window.location = "/hq/company/gstdetails"         
        }else if(event.target.id === 'companyConfig'){
            window.location = "/hq/company/company-config" 
        }
    }

    render() {
        let userName=localStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        return (
            <div className="Navigation">
                <ul>
                    <li><a id="dashboard" href="/dashboard/" onClick={this.redirect} className="navbar-brand">Dashboard</a></li>
                    <li><a id="trips" href="/trips/" onClick={this.redirect} className="navbar-brand">Trips</a></li>
                    <li><Link className="nav-link" to="/people">People</Link></li>
                    <li><a id="places" href="/places/" onClick={this.redirect} className="navbar-brand">Places</a></li>
                    <li><a id="company" href="#" onClick={this.showDropdownMenu} className="navbar-brand sub-navbar-brand">Company <Icon className="selectme" color="#36c" size={12} icon="down-arrow" /></a>
                    { this.state.displayMenu ? (
                        <ul className="subMenu" ref={(element) => {this.dropdownMenu = element ;}}>
                            <li><a id="companyConfig" href="#" onClick={this.redirect} className="navbar-brand">Company Config</a></li>
                            <li><a id="gst" href="#" onClick={this.redirect} className="navbar-brand">GST</a></li>
                        </ul>
                    ) : (
                        null
                    )
                    }
                    </li>
                </ul>
                <ul className="userLogin">
                    <li>{userName}</li>
                    <li><Link id="myTrips" className="nav-link" onClick={this.redirect} to="/login">My Trips</Link></li>
                    <li><Link id="account" className="nav-link" onClick={this.redirect} to="/login">My Account</Link></li>
                    <li><Link id="tellUs" className="nav-link" onClick={this.redirect} to="/login">Tell us what you Think</Link></li>
                    
                    <li><Link className="nav-link" to="/hq/logout" onClick={AuthenticationService.logout}>Logout</Link></li>
                </ul>
            </div>

        
        )
    }
}

export default MenuComponent