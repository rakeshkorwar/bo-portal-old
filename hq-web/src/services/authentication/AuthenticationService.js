import axios from 'axios'

const API_URL = 'http://localhost:8080'

export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
export const USER_AUTH_DATA = 'userAuthData'
export const URL_COMPANY = 'company'

class AuthenticationService {

    executeBasicAuthenticationService(username, password) {
        console.log('executeBasicAuthenticationService'+username)
        return axios.get(`${API_URL}/basicauth`,
            { headers: { authorization: this.createBasicAuthToken(username, password) } })
    }

    executeJwtAuthenticationService(username, password) {
        console.log(username);
        return axios.post(`${API_URL}/authenticate`, {
            username,
            password
        })
    }

    createBasicAuthToken(username, password) {
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    checkRegisterAutherization() {
        var authKey=this.getCookie('ct-auth');
        console.log('CT Key :'+authKey)
        return axios.post('http://192.168.45.153:9098/checkUsrAuth',authKey,{ headers: {
            'Access-Control-Allow-Origin': '*',
            }});
    }

    getCookie = function(name) {
        var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
        if (match) return match[2];
      }

    registerSuccessfulLogin(username, password) {
        console.log('Login User Name :'+username)
        localStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME,username);
        const myObj = {
            email: username,
            password:password
          };
        return axios.post('http://192.168.45.153:9098/checkUsrLogin',myObj,{ headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            }});
    }

    registerSuccessfulLoginForJwt(username, token) {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        return 'Bearer ' + token
    }
    
    logout() {
        document.cookie = "Apache= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "userid= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "ct-auth= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "_session_id= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "ct-auth-preferences= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "currency-pref= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "usermisc= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        localStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        localStorage.removeItem(USER_AUTH_DATA);
        localStorage.removeItem(URL_COMPANY);
        console.log('Loggedout Successfully.....')
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        console.log('isUserLoggedIn'+user)
        if (user === null) return true
        return true
    }

    getLoggedInUserName() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        console.log('getLoggedInUserName'+user)
        if (user === null) return ''
        return user
    }

    setupAxiosInterceptors(token) {
        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token
                }
                return config
            }
        )
    }
}

export default new AuthenticationService()