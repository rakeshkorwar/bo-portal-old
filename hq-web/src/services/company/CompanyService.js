import axios from 'axios'
class CompanyService {
    
    fetchCompanyDetails(searchRequest) {
        console.log("Domain name search request : "+JSON.stringify(searchRequest));
        //return axios.post('http://192.168.50.211:9098/getCmpConfByDomain',searchRequest);
        //return axios.post('http://localhost:3001/getCmpConfByDomain',searchRequest);

        const getCompanyConfigURL=process.env.REACT_APP_getcmpconfbydomain;
        console.log('getCompanyConfigURL  '+getCompanyConfigURL)
        return axios.post(getCompanyConfigURL,searchRequest);
    }

    addCompanyDetails(addReq) {
        //return axios.post('http://192.168.50.211:9098/addEmploye', addReq);
       // return axios.post('http://localhost:3001/addEmploye', addReq);

        const addCompanyConfigURL=process.env.REACT_APP_addemploye;
        console.log('addCompanyConfigURL  '+addCompanyConfigURL)
        return axios.post(addCompanyConfigURL,addReq);
    }

    editCompanyDetails(editReq) {
        //return axios.post('http://192.168.50.211:9098/updateCompConf', editReq);
        //return axios.post('http://localhost:3001/updateCompConf', editReq);

        const updateCompanyConfigURL=process.env.REACT_APP_updatecompconf;
        console.log('updateCompanyConfigURL  '+updateCompanyConfigURL)
        return axios.post(updateCompanyConfigURL,editReq);


    }

    editCompanyContactDetails(editReq) {
        //return axios.post('http://192.168.50.211:9098/updateCmpContDet', editReq);
        //return axios.post('http://localhost:3001/updateCmpContDet', editReq);
        
        const updateCompanyContactURL=process.env.REACT_APP_updatecmpcontdet;
        console.log('updateCompanyContactURL  '+updateCompanyContactURL)
        return axios.post(updateCompanyContactURL,editReq);

    }

}

export default new CompanyService()