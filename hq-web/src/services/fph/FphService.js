import axios from 'axios'

class FphService{

    updatePaxDetails(updatePaxDetailsReq) {
        return axios.put(
            'http://172.17.26.11:9031/trips', updatePaxDetailsReq,
            {
                headers: {
                    'Content Type': 'application/json',
                    'Accept': 'application/json',
                    'trip-version': 'V1'
                }
            });
    }
}

export default new FphService()