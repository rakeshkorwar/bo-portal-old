import axios from 'axios'
class GstService {
    
    fetchGstCompanyDetails(gstSearchReq) {
        console.log('fetchGstCompanyDetails Req '+JSON.stringify(gstSearchReq))

        //return axios.post('http://192.168.44.141:9098/getCompanyGstDtls', gstSearchReq);
        //return axios.post('http://localhost:3001/getGstCompanyData',gstSearchReq);
        
        const gstSearchURL=process.env.REACT_APP_gstcompanysearch;
        console.log('gstSearchURL  '+gstSearchURL)
        return axios.post(gstSearchURL,gstSearchReq);
    }

     addGstCompanyDetails(addGstReq) {
        console.log('addGstCompanyDetails Req '+JSON.stringify(addGstReq))
       // return axios.post('http://192.168.44.141:9098/updateCmpGstDetails', addGstReq);
        //return axios.post('http://localhost:3001/addGstCompanyData', addGstReq);

        const addgstURL=process.env.REACT_APP_adddgst;
        return axios.post(addgstURL,addGstReq);
    }

    editGstCompanyDetails(editReq) {
        console.log('editGstCompanyDetails Req '+JSON.stringify(editReq))
        //return axios.post('http://192.168.44.141:9098/updateCmpGstDetails', editReq);
        //return axios.post('http://localhost:3001/editGstCompanyData', editReq);

        const editgstURL=process.env.REACT_APP_editgst;
        return axios.post(editgstURL,editReq);
    }

    deleteGstCompanyDetails(deleteReq) {
        console.log('deleteGstCompanyDetails Req '+JSON.stringify(deleteReq))
        //return axios.post('http://192.168.44.141:9098/cmpGstDelete', deleteReq);
        //return axios.post('http://localhost:3001/deleteGstCompanyData', deleteReq);

        const delgstURL=process.env.REACT_APP_deletegst;
        return axios.post(delgstURL,deleteReq);
    }

}

export default new GstService()