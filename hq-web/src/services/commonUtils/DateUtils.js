import react from 'react-dom'

class DateUtils {

    ConvertIsoToSimpleDateFormat(isoDate) {
        // Format : DD/MM/YYYY
        const dob = new Date(isoDate);
        let year = dob.getUTCFullYear();
        let month = dob.getUTCMonth() + 1;
        month=month<10 ? '0'+month : month;
        let date = dob.getUTCDate();
        date=date<10 ? '0'+date : date;
        return date + "/" + month + "/" + year;
    }

}
export default new DateUtils()