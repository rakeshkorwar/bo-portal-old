import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LoginComponent from '../pages/authentication/LoginComponent';
import LogoutComponent from '../pages/authentication/LogoutComponent';
import AuthenticatedRoute from '../pages/authentication/AuthenticatedRoute';
import PeopleComponent from '../pages/people/PeopleComponent';
import ShowPeopleComponent from '../pages/people/ShowPeopleComponent';
import EditPeopleComponent from '../pages/people/EditPeopleComponent';
import CompanyComponent from '../pages/company/companyConfig/CompanyComponent';
import GstComponent from '../pages/company/gst/GstComponent';
import FPHComponent from '../pages/fphTrips/generic/FPHComponent';
import AuthenticationService from '../services/authentication/AuthenticationService';
import AccessDenied from '../pages/authentication/AccessDenied';

export const URL_COMPANY_CONFIG = 'companyConfig'
export const URL_COMPANY_GST = 'companyGst'
export const USER_AUTH_DATA = 'userAuthData'
export const URL_COMPANY = 'company'

class InstructorApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loginEnable: false,
            gstEnable: false,
            companyEnable: false,
            authUsrValid: false,
            roleAccess: [1234, 5678],
            compRoleEnable: false,
            gstRoleEnable:false

        };
    }

    render() {
        let comRoleId = 1234;
        let gstRoleId = 5678;
        var authResponse='';
        var url = window.location.href;
        var filename = url.substring(url.lastIndexOf('/') + 1);
        var urlString = "/" + filename;
        console.log('....Entered URL.....' + urlString);
        if (filename !== '') {
            localStorage.setItem(URL_COMPANY, urlString);

            if (urlString == '/company' || urlString == '/gstdetails' || urlString == '/company-config') {
                console.log('Usr Authrization Status: ' + authResponse.valid)
                console.log('....Entered URL.....' + urlString);
                this.state.loginEnable = false;
                AuthenticationService.checkRegisterAutherization()
                    .then(response => {
                         authResponse = response.data;
                        console.log('Usr Authrization Status: ' + authResponse.valid)
                        if (authResponse !== '' && authResponse.valid) {
                            console.log('Usr Authrization is SUCCESS: ')
                            this.state.authUsrValid = true;
                            let userData=localStorage.getItem(USER_AUTH_DATA);
                            if(!userData){
                            localStorage.setItem(USER_AUTH_DATA,authResponse);
                            }
                            if (urlString === '/company' || urlString == '/company-config') {
                                this.state.companyEnable = true;
                                const comp = authResponse.roles.find(x => x === comRoleId);
                            if (comp){
                                this.state.compRoleEnable = true;
                                console.log('Company Role Available..' + comp)
                            }
                            }else if (urlString === '/gstdetails') {
                                this.state.gstEnable = true;
                                const gst = authResponse.roles.find(x => x === gstRoleId);
                                if (gst) {
                                   this.state.gstRoleEnable = true;
                                    console.log('GST Role Available..' + gst)
                                }
                            }
                        } else {
                            this.state.loginEnable = true;
                            this.state.gstEnable = false;
                            this.state.authUsrValid = true;
                            this.state.compRoleEnable=false;
                            console.log('Usr Authrization is FAILED..........' + this.state.gstEnable)
                        }
                    }).catch(function (error) {
                        if (error.response) {
                            console.log(error.message);
                            /* this.state.loginEnable = true;
                            this.state.authUsrValid = false; */
                        }
                    });
            }else{
                this.state.loginEnable = true;
                //localStorage.removeItem(URL_COMPANY);
            }
           //if(this.state.authUsrValid){
            localStorage.setItem(USER_AUTH_DATA,authResponse);
            const comp = this.state.roleAccess.find(x => x === comRoleId);
            if (comp){
                this.state.compRoleEnable = true;
                console.log('Company Role Available..' + comp)
            }
            const gst = this.state.roleAccess.find(x => x === gstRoleId);
            if (gst) {
               this.state.gstRoleEnable = true;
                console.log('GST Role Available..' + gst)
            }
           /* }else{
            this.state.loginEnable = false;
            this.state.compRoleEnable = false;
            this.state.gstRoleEnable = false;
            //localStorage.removeItem(URL_COMPANY);
          } */
        }
        
        return (
            <>
                <Router>
                    <>
                        <Switch> 
                            <AuthenticatedRoute path="/" exact component={LoginComponent} />
                            <AuthenticatedRoute path="/hq/login" exact component={LoginComponent} />
                            <AuthenticatedRoute path="/hq/logout" exact component={LogoutComponent} />
                            <AuthenticatedRoute path="/hq/people" exact component={PeopleComponent} />
                            <AuthenticatedRoute path="/hq/company/gstdetails"
                            render={
                            () =>
                            this.state.gstRoleEnable ?
                            <GstComponent /> :
                            <AccessDenied />
                            }
                            />
                            <AuthenticatedRoute path="/hq/company"
                                render={
                                    () =>
                                        this.state.compRoleEnable ?
                                            <CompanyComponent /> :
                                            <AccessDenied />
                                }
                            />
                            <AuthenticatedRoute path="/hq/company/company-config"
                            render={
                            () =>
                            this.state.compRoleEnable ?
                            <CompanyComponent /> :
                            <AccessDenied />
                            }
                            />
                            <AuthenticatedRoute path="/hq/ShowPeopleComponent" exact component={ShowPeopleComponent} />
                            <AuthenticatedRoute path="/hq/EditPeopleComponent" exact component={EditPeopleComponent} />
                            <AuthenticatedRoute path="/hq/trips/fph/:tripId" exact component={FPHComponent} />
                        </Switch>
                    </>
                </Router>
            </>
        )
    }
}

export default InstructorApp