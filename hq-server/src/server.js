var CompanyNodeService = require('./services/company/CompanyNodeService.js');
var GstNodeService = require('./services/company/GstNodeService.js')
var FphNodeService = require('./services/FPH/FphNodeService.js')
var AuthNodeService = require('./services/authentication/AuthNodeService.js')

var cors = require('cors');

const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const API_PORT = 3001;
const app = express();
const router = express.Router();
const JSON = require('circular-json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
require('dotenv').config();

app.post('/getCmpConfByDomain', (req, res) => {
    CompanyNodeService.getCompanyDetails(req, function(response){
        var resp = response;
        res.json(JSON.stringify(resp));
    });
});

app.post('/updateCompConf', (req, res) => {
    CompanyNodeService.updateCompanyDetails(req, function(response){
        var resp = response;
        res.json(JSON.stringify(resp));
    });
});

app.post("/getGstCompanyData", (req, res) => {
    GstNodeService.getGstCompanySearch(req, function(response){
        var resp = response;
        res.json(JSON.stringify(resp));
    });
});

app.post("/editGstCompanyData", (req, res) => {
    GstNodeService.editGstCompany(req, function(response){
        var resp = response;
        res.json(JSON.stringify(resp));
    });
});

app.post("/deleteGstCompanyData", (req, res) => {
    GstNodeService.deleteGstDetails(req, function(response){
        var resp = response;
        res.json(JSON.stringify(resp));
    });
});

app.post("/addGstCompanyData", (req, res) => {
    GstNodeService.addGstCompanyData(req, function(response){
        var resp = response;
        res.json(JSON.stringify(resp));
    });
});

app.post("/checkUsrLogin", (req, res) => {
    AuthNodeService.authenticateUser(req, function(response){
        var resp = response;
        res.json(JSON.stringify(resp));
    });
});

app.use('/api', router);

app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));