const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const axios = require('axios');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();
const JSON = require('circular-json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

exports.authenticateUser = function (req, cb) {
    console.log("checkUsrLogin Request is " + JSON.stringify(req.body));
    axios
      .post("http://172.17.8.45:9001/externalapi/signin", req.body, {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
          Accept: "text/json",
          "X-Forwarded-Proto": "https"
        }
      })
      .then(function(response) {
        const jsondata = JSON.stringify(response);
        //res.send(jsondata);
        cb(jsondata);
      })
      .catch(error => {
        console.error("error in checkUsrLogin api is--- " + error);
      });
}