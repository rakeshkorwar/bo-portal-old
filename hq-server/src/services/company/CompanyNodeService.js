const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const axios = require('axios');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();
const JSON = require('circular-json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

exports.getCompanyDetails = function (req, cb) {
    const domain = req.body.domain_name;
    const comConfigSearchURL=process.env.companyconfigsearch;
    axios.get(comConfigSearchURL + domain + '&caller=air',
     { headers: { Accept: 'text/json' } })
    .then(function (response) {
        var resp = response.data;
        resp.status = response.status;
        resp.msg =  response.statusText;
        cb(resp);
    }).catch(function (error) {
        console.log(error);
        var resp = '';
        resp = {
            status: '404',
            msg:'Resource Not Found'
        }
        cb(resp);
    });
}

exports.updateCompanyDetails = function (req, cb){
    const companyId = req.body.companyId;
    const companyData = req.body;
    const comUpdatServiceURL=process.env.companyupdateservice;

    const contactData = req.body.company.contact_data;
    var cntReq={
        "contact_data":{
            addresses: contactData.addresses,
	        phone_numbers: contactData.phone_numbers,
            other_details: contactData.other_details,
            websites: contactData.websites,
            emails: contactData.emails,
            whatsapp_details: contactData.whatsapp_details
        }
    }
    console.log("Contact Data Update Request : " + JSON.stringify(cntReq));
    axios.post(comUpdatServiceURL + companyId + '/contact-data/update',
                            cntReq, { headers: { Accept: 'text/json' } })
    .then(function (response) {
        var resp = response.data;
        resp.status = response.status;
        resp.msg =  response.statusText;
        console.log('Company Config Contact Data Update Succesfull. Response Code : ' + resp.status);
    }).catch(function (error) {
        console.log(error);
        var resp = '';
        this.resp = {
            status: '404',
            msg:'Resource Not Found'
        }
        console.log('Company Config Contact Data Update Failed : ' + JSON.stringify(this.resp));
    });

    var cmpReq={
        "company":{
            name: companyData.company.name,
	        city: companyData.company.city,
            domain_name: companyData.company.domain_name,
            company_configs: companyData.company.company_configs
        }
    }
    console.log("Company Config Data Update Request : " + JSON.stringify(cmpReq));
    console.log('URL : ' + comUpdatServiceURL + companyId);
    axios.post(comUpdatServiceURL + companyId, cmpReq,
            { headers: { Accept: 'text/json' } })
    .then(function (response) {
        var resp = response.data;
        resp.status = response.status;
        resp.msg =  response.statusText;
        console.log('Company Config Data Update Succesfull. Response Code : ' + resp.status);
        cb(resp);
    }).catch(function (error) {
        console.log(error);
        var resp = '';
        this.resp = {
            status: '404',
            msg:'Resource Not Found'
        }
        console.log('Company Config Data Update Succesfull. Response Code : ' + resp);
        cb(resp);
    });
}