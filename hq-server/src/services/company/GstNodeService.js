const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const axios = require('axios');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();
const JSON = require('circular-json');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

exports.getGstCompanySearch = function (req, res) {
    console.log("getGstCompanyData request is--- " + JSON.stringify(req.body));
    const gstURL=process.env.gstcompanysearch;
    axios
      .get(
        gstURL +
          req.body.domain_name,
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(function(response) {
        console.log(
          "getGstCompanyData response is--- " + JSON.stringify(response)
        );         
        
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstResponse = response.data;
            gstResponse.responsecode = response.status;
            gstResponse.msg = "SUCCESS";
            res(gstResponse);
          } else {
            const gstsearchRes = "";
            this.gstsearchRes = {
              status: "404",
              msg: "CompanyNotFound"
            };
            res(this.gstsearchRes);
          }
        }
      })
      .catch(error => {
        console.log('error  '+JSON.stringify(error.response));
        const gstsearchRes = "";
        this.gstsearchRes = {
          status: "404",
          msg: "CompanyNotFound"
        };
        console.log(
          "getGstCompanyData response in catch block is--" +
            JSON.stringify(this.gstsearchRes)
        );
        res(this.gstsearchRes);
      });
}

exports.editGstCompany = function (req, res) {
    console.log("editGstCompanyData request is--- " + JSON.stringify(req.body));
    const editGstURL=process.env.editgst;
    axios
      .post(editGstURL, req.body)
      .then(function(response) {
        console.log(
          "editGstCompanyData response is--- " + JSON.stringify(response)
        );
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstEditResponse = response.data;
            gstEditResponse.responsecode = response.status;
            gstEditResponse.msg = "SUCCESS";
            res(gstEditResponse);
          } else {
            const gsteditRes = "";
            this.gsteditRes = {
              status: "404",
              msg: "CompanyNotFound"
            };
            res(this.gsteditRes);
          }
        }
      })
      .catch(error => {
        const gsteditRes = "";
        this.gsteditRes = {
          status: "404",
          msg: "CompanyNotFound"
        };
        console.log(
          "editGstCompanyData response in catch block is--" +
            JSON.stringify(this.gsteditRes)
        );
       
        res(this.gsteditRes);
      });
}

exports.deleteGstDetails = function (req, res) {
    console.log("deleteGstCompanyData request is--- " + JSON.stringify(req.body));
    //axios.delete("http://172.17.8.45:9001/companies/gst",req.body)--not working
    const deleGstURL=process.env.deletegst;
    axios({
      method: "DELETE",
      url: deleGstURL,
      data: req.body
    })
      .then(function(response) {
        console.log(
          "deleteGstCompanyData response is--- " + JSON.stringify(response)
        );
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstDelResponse = response.data;
            gstDelResponse.msg = "SUCCESS";
            res(gstDelResponse);
          } else {
            const gstdelRes = "";
            this.gstdelRes = {
              status: "404",
              msg: "CompanyNotFound"
            };
            res(this.gstdelRes);
          }
        }
      })
      .catch(error => {
        const gstdelRes = "";
        this.gstdelRes = {
          status: "404",
          msg: "CompanyNotFound"
        };
        console.log(
          "deleteGstCompanyData response in catch block is--" +
            JSON.stringify(this.gstdelRes)
        );
        res(this.gstdelRes);
      });
}

exports.addGstCompanyData = function (req, res) {
    console.log("addGstCompanyData request is--- " + JSON.stringify(req.body));
    const addGstURL=process.env.adddgst;
    axios
      .post(addGstURL, req.body)
      .then(function(response) {
        console.log(
          "addGstCompanyData response is--- " + JSON.stringify(response)
        );
        if (response !== "" && response !== undefined) {
          if (
            response.status !== "" &&
            response.status !== undefined &&
            response.status === 200
          ) {
            var gstAddResponse = response.data;
            gstAddResponse.responsecode = response.status;
            gstAddResponse.msg = "SUCCESS";
            res(gstAddResponse);
          } else {
            const gstaddRes = "";
            this.gstaddRes = {
              status: "404",
              msg: "CompanyNotFound"
            };
            res(this.gstaddRes);
          }
        }
      })
      .catch(error => {
        const gstaddRes = "";
        this.gstaddRes = {
          status: "404",
          msg: "CompanyNotFound"
        };
        console.log(
          "addGstCompanyData response in catch block is--" +
            JSON.stringify(this.gstaddRes)
        );
        res(this.gstaddRes);
      });
}